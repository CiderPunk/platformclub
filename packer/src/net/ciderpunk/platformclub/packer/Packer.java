package net.ciderpunk.platformclub.packer;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;

import java.lang.Exception;

public class Packer {

	public static void main (String[] arg) throws Exception{
		TexturePacker.Settings settings = new TexturePacker.Settings();
		TexturePacker.process(settings, "atlas/", "android/assets", "res");
	}
}
