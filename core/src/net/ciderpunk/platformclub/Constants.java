package net.ciderpunk.platformclub;

/**
 * Created by Matthew on 19/08/2015.
 */
public class Constants {

  public static final String AtlasPath = "res.atlas";
  //public static final String SkinPath = "res.json";

  public static final float CollisionTileSize = 1f;
  public static final float CollisionTileSize2 = CollisionTileSize * CollisionTileSize;
  public static final float UnitScale = 1f / 16f;
  public static final float Gravity = -20F;
  public static final String KeyPreferencesPath = "PlatformClub.keys";
}
