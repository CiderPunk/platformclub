package net.ciderpunk.platformclub.level.collision;

/**
 * Created by matthewlander on 07/10/15.
 */
public enum Direction {
	Up,
	Down,
	Left,
	Right,
}


