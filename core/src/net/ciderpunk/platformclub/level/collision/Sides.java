package net.ciderpunk.platformclub.level.collision;

/**
 * Created by matthewlander on 19/10/15.
 */
public class Sides {
	public static final int TOP = 1;
	public static final int RIGHT = 2;
	public static final int BOTTOM = 4;
	public static final int LEFT = 8;
}
