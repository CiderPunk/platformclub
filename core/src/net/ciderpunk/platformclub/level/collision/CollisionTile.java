package net.ciderpunk.platformclub.level.collision;

import com.badlogic.gdx.math.Vector2;
import net.ciderpunk.platformclub.ents.MapEntity;

/**
 * Created by matthewlander on 14/09/15.
 */
public abstract class CollisionTile {


	public enum CoarseTestResult {
		Hit,
		Miss,
		Complex,
	}

	protected CollisionTile(){};

	public abstract boolean collisionTest(Vector2 pos, MapEntity mapEntity, Vector2 offset, Direction direction);
	public abstract CoarseTestResult CourseTest(int Sides);
}
