package net.ciderpunk.platformclub.level.collision;

import com.badlogic.gdx.math.Vector2;
import net.ciderpunk.platformclub.Constants;
import net.ciderpunk.platformclub.ents.MapEntity;

/**
 * Created by matthewlander on 30/09/15.
 */
public class Solid extends CollisionTile {

	private static final Vector2 temp= new Vector2();

	public static final CollisionTile instance = new Solid();

	@Override
	public boolean collisionTest(Vector2 pos, MapEntity mapEntity, Vector2 offset, Direction direction) {
		//fine collision test not needed
		//correction calc
		//get difference in location

	}


	@Override
	public CoarseTestResult CourseTest(int Sides) {
		return CoarseTestResult.Hit;
	}
}
