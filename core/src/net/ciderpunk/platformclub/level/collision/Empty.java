package net.ciderpunk.platformclub.level.collision;

import com.badlogic.gdx.math.Vector2;
import net.ciderpunk.platformclub.ents.MapEntity;

/**
 * Created by matthewlander on 30/09/15.
 */
public class Empty extends CollisionTile {

	public static final CollisionTile instance = new Empty();

	@Override
	public boolean collisionTest(Vector2 pos, MapEntity mapEntity, Vector2 offset, Direction direction) {
		return false;
	}

	@Override
	public CoarseTestResult CourseTest(int Sides) {
		return CoarseTestResult.Miss;
	}
}
