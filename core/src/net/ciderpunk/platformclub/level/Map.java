package net.ciderpunk.platformclub.level;

import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.MapObjects;
import com.badlogic.gdx.maps.objects.TextureMapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.IntArray;
import net.ciderpunk.gamebase.ents.EntityLibrary;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.platformclub.Constants;
import net.ciderpunk.gamebase.ents.EntityList;
import net.ciderpunk.platformclub.ents.MapEntity;
import net.ciderpunk.platformclub.ents.Player;
import net.ciderpunk.platformclub.level.collision.CollisionTile;
import net.ciderpunk.platformclub.level.collision.Direction;
import net.ciderpunk.platformclub.level.collision.Empty;
import net.ciderpunk.platformclub.level.collision.Solid;

/**
 * Created by Matthew on 16/08/2015.
 */
public class Map implements IResourceUser{

  private final Vector2 tilePos = new Vector2();
  private final String levelPath;
  private TiledMap map;
  private TiledMapTileLayer collision;
  int[] drawLayers;
  //final static int CollisionHash = "collision".hashCode();
  OrthogonalTiledMapRenderer renderer;
  EntityList BluFor, RedFor;

  int width, height;


CollisionTile[][] collisionMap;

  public Map(String level){
    levelPath = level;
    BluFor = new EntityList();
    RedFor = new EntityList();
  }

  public EntityList getBluFor(){
    return BluFor;
  }

  public EntityList getRedFor(){
    return RedFor;
  }

  @Override
  public void preLoad(ResourceManager resMan) {
    if (resMan.getAssetMan().getLoader(TiledMap.class) == null) {
      resMan.getAssetMan().setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));
    }
    resMan.getAssetMan().load(levelPath, TiledMap.class);
  }

  @Override
  public void postLoad(ResourceManager resMan) {
    map = resMan.getAssetMan().get(levelPath);
  }

  public void initMap(){
    int i = 0;
    IntArray drawList = new IntArray();
    for (MapLayer layer : map.getLayers()){
      if (layer.getName().equals("collision")){
        initCollision((TiledMapTileLayer) layer);
      }
      else if (layer.getName().equals("ents")) {
        MapObjects objects = layer.getObjects();
        initEnts(objects);
      }
      else{
        drawList.add(i);
      }
      i++;
    }
    drawLayers = drawList.toArray();
    renderer = new OrthogonalTiledMapRenderer(map, Constants.UnitScale);
  }


  private void initCollision(TiledMapTileLayer layer) {
    width = layer.getWidth(); height = layer.getHeight();
    collisionMap = new CollisionTile[width][height];
    for (int y = 0; y < height; y++) {
      for (int x = 0; x < width; x++) {
        collisionMap[x][y] = getCollisionTile(x,y, layer);
      }
    }
  }

  private CollisionTile getCollisionTile(int x, int y, TiledMapTileLayer layer) {
    int tileId;
    try {
      TiledMapTile tile = layer.getCell(x, y).getTile();
      tileId  = Integer.parseInt((String) tile.getProperties().get("colTile"));
    }catch(Exception ex) {
      tileId = 0;
    }
    switch (tileId){
      case 1:
        return Solid.instance;
    }
    return Empty.instance;
  }

   private void initEnts(MapObjects objects) {
    for (int i = 0; i < objects.getCount(); i++){
      MapObject obj = objects.get(i);
      if (obj instanceof TextureMapObject) {
        TextureMapObject tmo = (TextureMapObject) obj;
        MapEntity ent = (MapEntity) EntityLibrary.getObject(tmo.getName());
        ent.init(this, tmo.getX() * Constants.UnitScale, tmo.getY() * Constants.UnitScale);
        //if (ClassReflection.isInstance(Player.class, ent)){
        if (ent instanceof Player) {
          BluFor.add(ent);
        } else {
          RedFor.add(ent);
        }
      }

    }
  }

  public void draw(OrthographicCamera cam){
    renderer.setView(cam);
    renderer.render(drawLayers);
  }

  public CollisionTile.CoarseTestResult courseTest(int x, int y, int sides) {
    if (x > 0 && y > 0 && x < width && y < height) {
     return (collisionMap[x][y].CourseTest(sides));
    }
    return CollisionTile.CoarseTestResult.Hit;
  }

}
