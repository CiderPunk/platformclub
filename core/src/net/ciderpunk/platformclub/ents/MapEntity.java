package net.ciderpunk.platformclub.ents;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.IntArray;
import net.ciderpunk.gamebase.ents.IEntity;
import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.platformclub.Constants;
import net.ciderpunk.platformclub.level.Map;
import net.ciderpunk.platformclub.level.collision.Direction;
import net.ciderpunk.platformclub.level.collision.Sides;

/**
 * Created by Matthew on 20/08/2015.
 */
public abstract class MapEntity implements IEntity {

  public Vector2 loc, vel;
  protected static final Vector2 offset = new Vector2();
  protected static final Vector2 absOffset = new Vector2();
  protected static final Vector2 newLoc = new Vector2();
  protected static final Vector2 temp = new Vector2();
  protected static final IntArray retestList = new IntArray();

  protected static Vector2 correction = new Vector2();
  public abstract Vector2 getBounds();
  protected Frame currentFrame;

  protected Map map;

  public void setMap(Map owner){
    map = owner;
  }
  public Map getMap(){
    return map;
  }


  public MapEntity(){
    loc = new Vector2();
    vel = new Vector2();
  }


  public void updatePosition(float dT) {
    offset.setZero().mulAdd(vel, dT);
    absOffset.set(Math.abs(offset.x), Math.abs(offset.y));
    if (absOffset.x > Constants.CollisionTileSize || absOffset.y > Constants.CollisionTileSize) {
      float longSide = (absOffset.x > absOffset.y ? absOffset.x : absOffset.y);
      float steps = MathUtils.ceilPositive(longSide / Constants.CollisionTileSize);
      offset.scl(1f / steps);
      for (int step = 0; step > steps; step++) {
        if (!doMove()){
          //do something with remaining steps
          break;
        }
      }
    }
    else {
      //single test/move
      doMove();
    }
  }


  protected boolean coarseTest(int x, int y, int sides, Direction direction){
    switch (map.courseTest(x, y, sides)) {
      case Complex:
        retestList.add(x);
        retestList.add(y);
      case Miss:
        return false;
      case Hit:
        temp.set(loc).sub(x, y);
        Vector2 bounds = getBounds();
        //correct offset according to test direction
        switch (direction) {
          case Down: {
            //ent above tile, positive diff
            offset.y += temp.y - bounds.y - Constants.CollisionTileSize;
            break;
          }
          case Up: {
            offset.y -= temp.y + bounds.y;
            break;
          }
          case Left: {
            offset.x+= temp.x - bounds.x - Constants.CollisionTileSize;
            break;
          }
          case Right: {
            offset.x -= temp.x + bounds.x;
            break;
          }
        }
        //attempt move again
        doMove();
        return true;
    }
    return false;
  }

  protected boolean doMove(){
    //loc.add(offset);
    newLoc.set(loc).add(offset);
    retestList.clear();
    int minX = MathUtils.floor((newLoc.x - this.getBounds().x) / Constants.CollisionTileSize);
    int maxX = MathUtils.floor((newLoc.x + this.getBounds().x) / Constants.CollisionTileSize);
    int minY = MathUtils.floor((newLoc.y - this.getBounds().y) / Constants.CollisionTileSize);
    int maxY = MathUtils.floor((newLoc.y + this.getBounds().y) / Constants.CollisionTileSize);
    //vertical movement test
    if (offset.y > 0){
      if (testCoarseHorizontal(minX, maxX, maxY, Sides.BOTTOM, Direction.Up)){
        return true;
      }
    }
    else if (offset.y < 0){
      if(testCoarseHorizontal(minX, maxX, minY, Sides.TOP, Direction.Down)){
        return true;
      }
    }
    //horizontal movement test
    if (offset.x > 0) {
      if(testCoarseVertical(maxX, minY, maxY,Sides.RIGHT, Direction.Right)){
        return true;
      }
    }
    else if (offset.x < 0) {
      if(testCoarseVertical(minX, minY, maxY, Sides.LEFT, Direction.Left)){
        return true;
      }
    }

    while(retestList.size > 0){
      int y = retestList.pop();
      int x = retestList.pop();
    }
    loc.set(newLoc);
  }

  protected  boolean testCoarseVertical(int x, int minY, int maxY, int side, Direction direction){
    for(int y = minY + 1; x < maxY; x++){
      if(coarseTest(x,y, side | Sides.TOP | Sides.BOTTOM, direction )){
        return true;
      }
    }
    if(coarseTest(x, minY, side | Sides.TOP, direction )){
      return true;
    }
    if(coarseTest(x, maxY, side | Sides.BOTTOM,direction)){
      return true;
    }
    //no correction
    return false;
  }

  private boolean testCoarseHorizontal(int minX, int maxX, int y, int side, Direction direction) {
    for(int x = minX + 1; x < maxX; x++){
      if(coarseTest(x,y, side | Sides.LEFT | Sides.RIGHT, direction)){
        return true;
      }
    }
    if(coarseTest(minX,y, side | Sides.RIGHT, direction )){
      return true;
    }
    if(coarseTest(maxX,y, side | Sides.LEFT,direction)){
      return true;
    }
    //no correction
    return false;
  }

  protected void applyGravity(float dT){
    vel.y += Constants.Gravity * dT;
  }

  public void init(Map owner, float x, float y){
    map = owner;
    loc.set(x,y);
  }

  @Override
  public void cleanUp() {
  }

  @Override
  public void draw(SpriteBatch batch) {
    currentFrame.draw(batch, loc.x, loc.y);
  }

  @Override
  public void debugDraw(ShapeRenderer renderer) {
    renderer.begin(ShapeRenderer.ShapeType.Line);
    renderer.setColor(1, 1, 0, 1);
    renderer.rect(this.loc.x - this.getBounds().x, this.loc.y - this.getBounds().y, this.getBounds().x * 2, this.getBounds().y * 2);
    renderer.end();
  }

  public void correctPosition(float xCorrect, float yCorrect, boolean horizStop, boolean verticalStop) {
    correction.x = (Math.abs(xCorrect) > Math.abs(correction.x) ? xCorrect : correction.x);
    correction.y = (Math.abs(yCorrect) > Math.abs(correction.y) ? yCorrect : correction.y);
    if (horizStop){
      vel.x = 0f;
    }
    if (verticalStop){
      vel.y = 0f;
    }
  }
}
