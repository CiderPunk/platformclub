package net.ciderpunk.platformclub.ents;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import net.ciderpunk.gamebase.controls.Action;
import net.ciderpunk.gamebase.controls.IActionConsumer;
import net.ciderpunk.gamebase.ents.EntityLibrary;
import net.ciderpunk.gamebase.gfx.Frame;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.platformclub.Constants;
import net.ciderpunk.platformclub.gui.PlatformGameScreen;
import net.ciderpunk.platformclub.level.Map;

/**
 * Created by Matthew on 19/08/2015.
 */
public class Player extends MapEntity implements IActionConsumer {

  private static final PlayerAction actionJump = new PlayerAction("Jump", "pl_jump", 19, PlayerMove.jump);
  private static final PlayerAction actionLeft = new PlayerAction("Move left", "pl_left", 21, PlayerMove.left);
  private static final PlayerAction actionRight = new PlayerAction("Move right", "pl_right", 22, PlayerMove.right);


  public static class PlayerAction extends Action{
    public final PlayerMove move;
    public PlayerAction(String name, String key, int defKey, PlayerMove move) {
      super(name, key, defKey);
      this.move = move;
    }
  }

  protected enum PlayerMove{
    jump,
    left,
    right,
    duck,
    shoot,
  }

  //actions
  static{
    PlatformGameScreen.getKeyMap().registerActions(new Action[]{ actionJump, actionLeft, actionRight });
  }


  public Player() {
    super();
    actionJump.addConsumer(this);
    actionLeft.addConsumer(this);
    actionRight.addConsumer(this);
  }

  @Override
  public void cleanUp() {
    super.cleanUp();

    actionJump.removeConsumer(this);
    actionLeft.removeConsumer(this);
    actionRight.removeConsumer(this);

  }

  @Override
  public boolean handleKeyDown(Action action) {

    switch (((PlayerAction)action).move){
      case jump:
        this.vel.y = 10f;
        break;
      case left:
        this.vel.x = -5f;
        break;
      case right:
        this.vel.x = 5f;

    }
    return false;
  }

  @Override
  public boolean handleKeyUp(Action action) {
    return false;
  }

  static Frame left, right;
  private static final Vector2 bounds = new Vector2(1f,1.5f);

  static{
    EntityLibrary.RegisterClass("player", Player.class);
  }

  @Override
  public void init(Map owner, float x, float y) {
    super.init(owner, x, y);
    this.currentFrame = right;
  }

  @Override
  public Vector2 getBounds() {
    return bounds;
  }

  @Override
  public boolean update(float dT) {
    applyGravity(dT);
    updatePosition(dT);
    return true;
  }


  @Override
  public void draw(SpriteBatch batch) {
    super.draw(batch);
  }


  public static class Loader implements IResourceUser{
    @Override
    public void preLoad(ResourceManager resMan) {
      resMan.getAssetMan().load(Constants.AtlasPath, TextureAtlas.class);
    }

    @Override
    public void postLoad(ResourceManager resMan) {
      TextureAtlas atlas = resMan.getAssetMan().get(Constants.AtlasPath, TextureAtlas.class);
      left = new Frame(atlas.findRegion("dude/left"), 16F, 24F, Constants.UnitScale);
      right = new Frame(atlas.findRegion("dude/right"), 16F, 24F, Constants.UnitScale);
    }

  }
}
