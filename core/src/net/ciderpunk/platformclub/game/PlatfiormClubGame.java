package net.ciderpunk.platformclub.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import net.ciderpunk.gamebase.events.IListener;
import net.ciderpunk.gamebase.game.GameManager;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.platformclub.Constants;
import net.ciderpunk.platformclub.ents.Player;
import net.ciderpunk.platformclub.gui.PlatformGameScreen;
import net.ciderpunk.platformclub.level.Map;

/**
 * Created by Matthew on 16/08/2015.
 */
public class PlatfiormClubGame extends GameManager implements IListener<ResourceManager> {


  Map currentLevel;


  PlatformGameScreen gameScreen;


  @Override
  public void update() {
    super.update();
  }

  @Override
  public void draw(float dt) {
    Gdx.graphics.getGL20().glClearColor(0.2f, 0.2f, 0.3f, 0);
    Gdx.graphics.getGL20().glClear(GL20.GL_COLOR_BUFFER_BIT );
    super.draw(dt);
  }

  @Override
  protected void think(float dt) {
    super.think(dt);
  }

  public PlatfiormClubGame(){
    super (new ExtendViewport(Constants.UnitScale * 640, Constants.UnitScale  *480));
    gameScreen = new PlatformGameScreen(this.viewport);

  }

  @Override
  public void preLoad(ResourceManager resMan) {
    currentLevel = new Map("levels/level1.tmx");

    resMan.addResourceUser(currentLevel);
    resMan.addResourceUser(new Player.Loader());
    resMan.addListener(this);

  }

  @Override
  public void postLoad(ResourceManager resMan) {
    currentLevel.initMap();
  }

  @Override
  public void doEvent(ResourceManager source) {
    gameScreen.initLevel(currentLevel);
    stage.addActor(gameScreen);
    Gdx.input.setInputProcessor(stage);
    stage.setKeyboardFocus(gameScreen);
  }

}
