package net.ciderpunk.platformclub;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import net.ciderpunk.platformclub.game.PlatfiormClubGame;

public class PlatformClub extends ApplicationAdapter {
	PlatfiormClubGame game;
  @Override
	public void create () {  game  = new PlatfiormClubGame();	}
	@Override
	public void render () {		game.update();	}

  @Override
  public void dispose() {
    game.dispose();
    super.dispose();
  }

  @Override
  public void resize(int width, int height) { game.resize(width, height); }
}
