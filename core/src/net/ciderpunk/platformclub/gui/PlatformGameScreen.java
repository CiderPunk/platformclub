package net.ciderpunk.platformclub.gui;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.Viewport;
import net.ciderpunk.gamebase.gui.GameScreen;
import net.ciderpunk.gamebase.controls.ActionMap;
import net.ciderpunk.gamebase.resources.IResourceUser;
import net.ciderpunk.gamebase.resources.ResourceManager;
import net.ciderpunk.platformclub.Constants;
import net.ciderpunk.platformclub.level.Map;

/**
 * Created by Matthew on 16/08/2015.
 */
public class PlatformGameScreen extends GameScreen implements Disposable {

  Camera cam;
  Map lvl;

  SpriteBatch spriteBatch;
  ShapeRenderer shapeRend;

  protected static ActionMap keymap = new ActionMap();
  public static ActionMap getKeyMap(){
    return keymap;
  }


  public PlatformGameScreen(Viewport viewport) {
    super();
    cam = viewport.getCamera();
    spriteBatch = new SpriteBatch();
    shapeRend = new ShapeRenderer();

    this.addListener(new ClickListener(){
      @Override
      public boolean keyDown(InputEvent event, int keycode) {

        return keymap.keyDown(keycode);

      }

      @Override
      public boolean keyUp(InputEvent event, int keycode) {
        return keymap.keyUp(keycode);
      }
    });
  }

  public void initLevel(Map level){
    lvl = level;


    keymap.loadConfig(Constants.KeyPreferencesPath);
    keymap.saveConfig(Constants.KeyPreferencesPath);

  }

  @Override
  public void act(float delta) {
    super.act(delta);

    if (delta > 0.1){delta = 0;}
    lvl.getBluFor().doUpdate(delta);
    lvl.getRedFor().doUpdate(delta);
  }

  @Override
  public void draw(Batch batch, float parentAlpha) {
    batch.end();

    if (lvl!= null){
      super.draw(batch, parentAlpha);
      lvl.draw((OrthographicCamera) cam);
      spriteBatch.setProjectionMatrix(cam.combined);
      spriteBatch.begin();
      lvl.getBluFor().doDraw(spriteBatch);
      lvl.getRedFor().doDraw(spriteBatch);
      spriteBatch.end();

      shapeRend.setProjectionMatrix(cam.combined);
      lvl.getBluFor().doDebugDraw(shapeRend);
      lvl.getRedFor().doDebugDraw(shapeRend);
    }
    batch.begin();
  }



  @Override
  protected void setStage(Stage stage) {
    super.setStage(stage);
    updateSize(stage);
  }



  public void updateSize(Stage stage){
    if (stage!= null) {
      this.setWidth(stage.getWidth());
      this.setHeight(stage.getWidth());
    }
  }

  @Override
  public void dispose() {

  }

}


